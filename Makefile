docker-delete-all: ## Delete all Docker files, use with caution
	@echo "\033[92mThis will delete all Docker containers, images & volumes on your computer! \033[0m"
	./bin/delete_all_docker.sh

